package com.kata.service;

import com.kata.dto.CountryDTO;
import com.kata.mapper.CountryMapper;
import com.kata.model.Country;
import com.kata.repository.CountryRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * @ In the name of Allah, most gracious and most merciful! 16.11.2022
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class CountryServiceImpl implements CountryService {
    private final CountryRepository countryRepository;
    private final CountryMapper countryMapper;

    @Override
    public List<CountryDTO> getAllCountries() {
        log.info("Fetch all countries");
        return countryMapper.modelsToDTOs(countryRepository.findAllNotRemoved());
    }

    @Override
    public CountryDTO getCountry(Long countryId) {
        log.info("Country with id = {} was found", countryId);
        return countryMapper.modelToDto(countryRepository.findCountryByIdIfNotRemoved(countryId));
    }

    @Override
    @Transactional
    public void addCountry(CountryDTO countryDTO) {
        Country country = countryMapper.dtoToModel(countryDTO);
        country.setIsSystem(false);
        country.setIsRemoved(false);
        countryRepository.save(country);
        log.info("Country:{} was saved", country);
    }

    @Override
    @Transactional
    public void update(Long id, CountryDTO countryDTO) {
        Country fromDto = countryMapper.dtoToModel(countryDTO);
        Country newCountry = countryRepository.findCountryByIdIfNotRemoved(id);
        newCountry.setIsSystem(fromDto.getIsSystem());
        newCountry.setShortName(fromDto.getShortName());
        newCountry.setFullName(fromDto.getFullName());
        newCountry.setNumericCode(fromDto.getNumericCode());
        newCountry.setAlpha2(fromDto.getAlpha2());
        newCountry.setAlpha3(fromDto.getAlpha3());
        newCountry.setIsRemoved(false);
        countryRepository.save(newCountry);
        log.info("Country: {} with id: {} was updated", id, newCountry);
    }

    @Override
    @Transactional
    public void deleteCountry(Long countryId) {
        countryRepository.markCountryAsRemoved(countryId);
        log.info("Country with id: {} was marked as removed", countryId);
    }
}
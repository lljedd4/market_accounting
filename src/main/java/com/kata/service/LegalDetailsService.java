package com.kata.service;

import com.kata.dto.LegalDetailsDto;
import com.kata.model.LegalDetails;

import java.util.List;

public interface LegalDetailsService {

    Object getById(Long id);

    void create(LegalDetailsDto legalDetailsDto);

    void update(Long id, LegalDetailsDto legalDetailsDto);

    void delete(Long id);
    List<LegalDetails> findAll();

    LegalDetails findById(Long id);

}

package com.kata.dto;

import com.kata.model.Employee;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class VatDto {

    private Long id;

    private Boolean isSystem;

    private Byte rate;

    private String comment;

    private LocalDateTime lastEdited;

    private Employee lastEditedBy;
}

package com.kata.service;

import com.kata.dto.VatDto;
import com.kata.mapper.VatMapper;
import com.kata.model.Vat;
import com.kata.repository.VatRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class VatServiceImpl implements VatService {

    private final VatRepository vatRepository;

    @Override
    public List<VatDto> findAll() {
        log.info("All VAT rates was found");
        return VatMapper.INSTANCE.toDtoList(vatRepository.findAllNotRemoved());
    }

    @Override
    public VatDto findById(Long id) {
        log.info("VAT rate with id = {} was found", id);
        return VatMapper.INSTANCE.toDto(vatRepository.findVatByIdIfNotRemoved(id));
    }

    @Override
    @Transactional
    public void save(VatDto vatDto) {
        Vat vat = VatMapper.INSTANCE.toEntity(vatDto);
        vat.setLastEdited(LocalDateTime.now());
        vat.setIsSystem(false);
        vat.setIsRemoved(false);
        vatRepository.save(vat);
        log.info("VAT rate was saved: {}", vat);
    }

    @Override
    @Transactional
    public void update(Long id, VatDto vatDto) {
        Vat fromDto = VatMapper.INSTANCE.toEntity(vatDto);
        Vat newVat = vatRepository.findVatByIdIfNotRemoved(id);
        newVat.setIsSystem(false);
        newVat.setIsRemoved(false);
        newVat.setLastEdited(LocalDateTime.now());
        newVat.setRate(fromDto.getRate());
        newVat.setComment(fromDto.getComment());
        newVat.setLastEditedBy(fromDto.getLastEditedBy());
        vatRepository.save(newVat);
        log.info("VAT rate was updated: {}", newVat);
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        vatRepository.markVatAsRemoved(id);
        log.info("VAT rate with id = {} was marked as removed", id);
    }
}

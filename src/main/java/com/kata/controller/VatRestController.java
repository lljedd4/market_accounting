package com.kata.controller;

import com.kata.dto.VatDto;
import com.kata.service.VatService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/catalog/vat")
@Api(value = "/vat", tags = "Ставки НДС")
public class VatRestController {

    private final VatService vatService;

    @ApiOperation(value = "Получение всех ставок НДС",
            notes = "Возвращает объекты всех ставок НДС")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Все ставки НДС получены")
    })
    @GetMapping
    public ResponseEntity<List<VatDto>> getAllVats() {
        return new ResponseEntity<>(vatService.findAll(), HttpStatus.OK);
    }

    @ApiOperation(value = "Получение ставки НДС",
            notes = "Принимает id ставки из URL и возвращает объект ставки НДС по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ставка НДС получена"),
            @ApiResponse(code = 404, message = "Ставка с таким id не найдена")
    })
    @GetMapping("/{id}")
    public ResponseEntity<VatDto> getVatById(@PathVariable("id") @ApiParam(name = "id", value = "Id ставки НДС", example = "1") Long id) {
        return new ResponseEntity<>(vatService.findById(id), HttpStatus.OK);
    }

    @ApiOperation(value = "Создание пользовательской ставки НДС",
            notes = "Принимает объект ставки НДС. Поля: id, isSystem, lastEdited, lastEditedBy должны быть null")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Пользовательская ставка НДС создана"),
            @ApiResponse(code = 406, message = "Запись с таким значением налоговой ставки уже существует")
    })
    @PostMapping
    public ResponseEntity<HttpStatus> createVat(@RequestBody @ApiParam(name = "Объект", value = "Объект ставки НДС для создания") VatDto vatDto) {
        vatService.save(vatDto);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @ApiOperation(value = "Редактирование пользовательской ставки НДС",
            notes = "Принимает id из URL и объект ставки НДС. Поля: id, isSystem, lastEdited, lastEditedBy должны быть null")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ставка НДС изменена"),
            @ApiResponse(code = 406, message = "Запись с таким значением налоговой ставки уже существует")
    })
    @PatchMapping("/{id}")
    ResponseEntity<HttpStatus> updateVat(@PathVariable("id") @ApiParam(name = "id", value = "Id ставки НДС") Long id,
            @RequestBody @ApiParam(name = "Ставка НДС", value = "Объект ставки НДС для редактирования") VatDto vatDto) {
        vatService.update(id, vatDto);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @ApiOperation(value = "Удаление пользовательской ставки НДС",
            notes = "Принимает id ставки из URL и удаляет ставку НДС по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ставка НДС удалена"),
            @ApiResponse(code = 404, message = "Ставка с таким id не найдена")
    })
    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> deleteVatById(@PathVariable("id") @ApiParam(name = "id", value = "Id ставки НДС для удаления", example = "1") Long id) {
        vatService.deleteById(id);
        return ResponseEntity.ok(HttpStatus.OK);
    }
}

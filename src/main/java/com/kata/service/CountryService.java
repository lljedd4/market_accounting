package com.kata.service;

import com.kata.dto.CountryDTO;

import java.util.List;

/**
 * @ In the name of Allah, most gracious and most merciful! 16.11.2022
 */
public interface CountryService {
    List<CountryDTO> getAllCountries();

    CountryDTO getCountry(Long countryId);

    void addCountry(CountryDTO countryDTO);

    void update(Long id, CountryDTO countryDTO);

    void deleteCountry(Long countryId);
}

package com.kata.service;

import com.kata.dto.UnitRequestDto;
import com.kata.dto.UnitResponseDto;
import com.kata.mapper.UnitMapper;
import com.kata.model.Unit;
import com.kata.repository.UnitRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class UnitServiceImpl implements UnitService {
    private final UnitRepository repository;
    private final UnitMapper mapper;

    @Override
    public List<UnitResponseDto> getAllUnits() {
        List<Unit> units = repository.findAll().stream().filter(unit -> !unit.isRemoved()).toList();
        log.info("All units of measurement have been received");
        return mapper.toDtoList(units);
    }

    @Override
    public UnitResponseDto findById(Long id) {
        log.info("Unit of measurement with id = {} was received", id);

        return mapper.toDto(repository.findById(id));
    }

    @Override
    @Transactional
    public void create(UnitRequestDto unitDto) {
        Unit unit = mapper.toUnit(unitDto);
        unit.setChangeable(true);

        repository.save(unit);

        log.info("Unit of measurement was created: {}", unit);
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        repository.markUnitAsRemoved(id);

        log.info("Unit of measurement with id = {} was marked as removed", id);
    }

    @Override
    @Transactional
    public void edit(Long id, UnitRequestDto unitDto) {
        Unit unit = repository.findById(id);

        if (!unit.isRemoved() && unit.isChangeable()) {
            unit.setShortName(unitDto.getShortName());
            unit.setFullName(unitDto.getFullName());
            unit.setDigitalCode(unitDto.getDigitalCode());

            repository.save(unit);

            log.info("Unit of measurement was updated: {}", unit);
        }
    }

}

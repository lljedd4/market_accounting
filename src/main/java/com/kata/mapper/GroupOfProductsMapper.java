package com.kata.mapper;


import com.kata.dto.GroupOfProductsDTO;
import com.kata.model.GroupOfProducts;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface GroupOfProductsMapper {
    GroupOfProductsMapper INSTANCE = Mappers.getMapper(GroupOfProductsMapper.class);
    List<GroupOfProductsDTO> toDtoList(List<GroupOfProducts> groupOfProducts);
    GroupOfProductsDTO toDto(GroupOfProducts groupOfProducts);
    GroupOfProducts toEntity(GroupOfProductsDTO groupOfProductsDTO);
}

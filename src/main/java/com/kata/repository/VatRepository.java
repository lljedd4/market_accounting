package com.kata.repository;

import com.kata.model.Vat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface VatRepository extends JpaRepository<Vat, Long> {

    @Query("SELECT v FROM Vat v WHERE v.isRemoved = false")
    List<Vat> findAllNotRemoved();

    @Query("SELECT v FROM Vat v WHERE v.id = :id AND v.isRemoved = false")
    Vat findVatByIdIfNotRemoved(Long id);

    @Modifying
    @Query("UPDATE Vat SET isRemoved = true WHERE id = :id")
    void markVatAsRemoved(Long id);

}

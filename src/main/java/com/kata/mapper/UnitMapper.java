package com.kata.mapper;

import com.kata.dto.UnitRequestDto;
import com.kata.dto.UnitResponseDto;
import com.kata.model.Unit;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UnitMapper {

    Unit toUnit(UnitRequestDto dto);

    UnitResponseDto toDto(Unit unit);

    List<UnitResponseDto> toDtoList(List<Unit> units);

}

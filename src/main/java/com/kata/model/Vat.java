package com.kata.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Vat {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Boolean isSystem;

    private Byte rate;

    private String comment;

    private LocalDateTime lastEdited;

    @ManyToOne
    @JoinColumn()
    private Employee lastEditedBy;

    private Boolean isRemoved;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Vat)) return false;
        Vat vat = (Vat) o;
        return Objects.equals(getIsSystem(), vat.getIsSystem()) &&
                Objects.equals(getRate(), vat.getRate()) &&
                Objects.equals(getComment(), vat.getComment()) &&
                Objects.equals(getLastEdited(), vat.getLastEdited()) &&
                Objects.equals(getLastEditedBy(), vat.getLastEditedBy()) &&
                Objects.equals(getIsRemoved(), vat.getIsRemoved());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getIsSystem(), getRate(), getComment(), getLastEdited(), getLastEditedBy(), getIsRemoved());
    }

    @Override
    public String toString() {
        return "Vat{" +
                "isSystem=" + isSystem +
                ", id=" + id +
                ", rate=" + rate +
                ", comment='" + comment + '\'' +
                ", lastEdited=" + lastEdited +
                ", lastEditedBy=" + lastEditedBy +
                ", isRemoved=" + isRemoved +
                '}';
    }
}

package com.kata.mapper;

import com.kata.dto.LegalDetailsDto;
import com.kata.model.LegalDetails;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface LegalDetailsMapper {
    LegalDetailsMapper INSTANCE = Mappers.getMapper(LegalDetailsMapper.class);
    LegalDetailsDto toDto(LegalDetails legalDetails);
    LegalDetails toEntity(LegalDetailsDto legalDetailsDto);

    List<LegalDetails> toDtoList(List<LegalDetails> legalDetails);
}

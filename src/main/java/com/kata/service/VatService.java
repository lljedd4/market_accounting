package com.kata.service;

import com.kata.dto.VatDto;

import java.util.List;

public interface VatService {

    List<VatDto> findAll();

    VatDto findById(Long id);

    void save(VatDto vatDto);

    void update(Long id, VatDto vatDto);

    void deleteById(Long id);
}

package com.kata.repository;

import com.kata.model.IpNetwork;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IpNetworkRepository extends JpaRepository<IpNetwork, Long> {
}

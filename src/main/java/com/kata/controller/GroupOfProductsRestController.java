package com.kata.controller;

import com.kata.dto.GroupOfProductsDTO;
import com.kata.service.GroupOfProductsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
//@AllArgsConstructor
@RequestMapping("/api/goods/groups")
@Api(value = "Controller creating a product group", tags = "Controller creating a product group")
public class GroupOfProductsRestController {

    private final GroupOfProductsService groupOfProductsService;

    @ApiOperation(value = "Получение всех групп товаров",
            notes = "Возвращает объекты всех групп товаров")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Все группы товаров получены")})
    @GetMapping //Выпадающее окно с выбором из существующих групп товаров, какой URI поставить?
    public ResponseEntity<List<GroupOfProductsDTO>> getAllGroups() {
        return new ResponseEntity<>(groupOfProductsService.findAll(), HttpStatus.OK);
    }


    @ApiOperation(value = "Получение группы товаров", notes = "Принимает id группы из URL и возвращает объект группы по id")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Группа товаров получена"),
    @ApiResponse(code = 404, message = "Группа с таким id не найдена")})
    @GetMapping("/{id}")
    public ResponseEntity<GroupOfProductsDTO> getGroupById(@PathVariable("id") @ApiParam(name = "id", value = "id группы товаров", example = "1") Long idGroup){
        return new ResponseEntity<>(groupOfProductsService.findById(idGroup), HttpStatus.OK);
    }


    @ApiOperation(value = "Создание группы товаров", notes = "Принимает объект ставки НДС. Поля: id, isSystem, lastEdited, lastEditedBy должны быть null")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Группа товаров создана"),
            @ApiResponse(code = 406, message = "Группа с таким названием уже существует")})
    @PostMapping
    public ResponseEntity<HttpStatus> createGroup(@RequestBody @ApiParam(name = "Объект", value = "Объект группы товаров для создания") GroupOfProductsDTO groupOfProductsDTO) {
        groupOfProductsService.save(groupOfProductsDTO);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @ApiOperation(value = "Редактирование группы товаров", notes = "Принимает id из URL и объект группы товаров. " +
            "Поля: id, isSystem, lastEdited, lastEditedBy должны быть null")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Группа товаров изменена"),
            @ApiResponse(code = 406, message = "Группа с таким названием уже существует")})
    @PatchMapping("/{id}")
    ResponseEntity<HttpStatus> updateGroup(@PathVariable("id") @ApiParam(name = "id", value = "Id группы товаров") Long idGroup,
                                           @RequestBody @ApiParam(name = "Группа товаров", value = "Объект группы товаров для редактирования") GroupOfProductsDTO groupOfProductsDTO) {
        groupOfProductsService.update(idGroup, groupOfProductsDTO);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @ApiOperation(value = "Удаление группы", notes = "Принимает id группы из URL и удаляет группу по id")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Группа удалена"),
            @ApiResponse(code = 404, message = "Группа с таким id не найдена")})
    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> deleteGroupbyId(@PathVariable("id") @ApiParam(name = "id",
            value = "Id группа товаров для удаления", example = "1") Long idGroup) {
        groupOfProductsService.deleteById(idGroup);
        return ResponseEntity.ok(HttpStatus.OK);
    }

}

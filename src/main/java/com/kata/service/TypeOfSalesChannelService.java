package com.kata.service;

import com.kata.model.TypeOfSalesChannel;

import java.util.List;

public interface TypeOfSalesChannelService {
    List<TypeOfSalesChannel> findAllTypeOfSalesChannel();
}

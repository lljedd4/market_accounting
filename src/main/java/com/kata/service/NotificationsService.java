package com.kata.service;

import com.kata.model.Notifications;

import java.util.List;

public interface NotificationsService {

    List<Notifications> findAllNotifications();
}

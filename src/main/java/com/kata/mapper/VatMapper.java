package com.kata.mapper;

import com.kata.model.Vat;
import com.kata.dto.VatDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface VatMapper {

    VatMapper INSTANCE = Mappers.getMapper(VatMapper.class);

    List<VatDto> toDtoList(List<Vat> vats);

    VatDto toDto(Vat vat);

    Vat toEntity(VatDto vatDto);
}

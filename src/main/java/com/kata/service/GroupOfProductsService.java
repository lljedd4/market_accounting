package com.kata.service;
import com.kata.dto.GroupOfProductsDTO;

import java.util.List;

public interface GroupOfProductsService {

    List<GroupOfProductsDTO> findAll();

    GroupOfProductsDTO findById(Long idGroup);

    void save(GroupOfProductsDTO groupOfProductsDTO);

    void update(Long idGroup, GroupOfProductsDTO groupOfProductsDTO);

    void deleteById(Long idGroup);

}

package com.kata.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "legaldetails")
public class LegalDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "type_of_counterparty", nullable = false)
    private String typeOfCounterparty;

    @Column(name = "inn", nullable = false)
    private Long inn;

    @Column(name = "full_name", nullable = false)
    private String fullName;

    @Column(name = "legal_address", nullable = false)
    private String legalAddress;

    @Column(name = "comment_to_the_address", nullable = false)
    private String commentToTheAddress;

    @Column(name = "kpp", nullable = false)
    private Long kpp;

    @Column(name = "ogrn", nullable = false)
    private Long ogrn;

    @Column(name = "okpo", nullable = false)
    private Long okpo;

    @Column(name = "is_removed")
    private boolean isRemoved;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LegalDetails that = (LegalDetails) o;
        return isRemoved == that.isRemoved && Objects.equals(typeOfCounterparty, that.typeOfCounterparty) && Objects.equals(inn, that.inn) && Objects.equals(fullName, that.fullName) && Objects.equals(legalAddress, that.legalAddress) && Objects.equals(commentToTheAddress, that.commentToTheAddress) && Objects.equals(kpp, that.kpp) && Objects.equals(ogrn, that.ogrn) && Objects.equals(okpo, that.okpo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(typeOfCounterparty, inn, fullName, legalAddress, commentToTheAddress, kpp, ogrn, okpo, isRemoved);
    }

    @Override
    public String toString() {
        return "LegalDetails{" +
                "id=" + id +
                ", typeOfCounterparty='" + typeOfCounterparty + '\'' +
                ", inn=" + inn +
                ", fullName='" + fullName + '\'' +
                ", legalAddress='" + legalAddress + '\'' +
                ", commentToTheAddress='" + commentToTheAddress + '\'' +
                ", kpp=" + kpp +
                ", ogrn=" + ogrn +
                ", okpo=" + okpo +
                ", isRemoved=" + isRemoved +
                '}';
    }
}

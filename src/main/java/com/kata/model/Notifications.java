package com.kata.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Notifications {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Boolean customerOrders;
    private Boolean customerOrdersEmail;
    private Boolean customerOrdersTelephone;

    private Boolean customerAccounts;
    private Boolean customerAccountsEmail;
    private Boolean customerAccountsTelephone;

    private Boolean remnants;
    private Boolean remnantsEmail;
    private Boolean remnantsTelephone;

    private Boolean retailTrade;
    private Boolean retailTradeEmail;
    private Boolean retailTradeTelephone;

    private Boolean tasks;
    private Boolean tasksEmail;
    private Boolean tasksTelephone;

    private Boolean dataExchange;
    private Boolean dataExchangeEmail;
    private Boolean dataExchangeTelephone;

    private Boolean scenariosEmail;
    private Boolean scenariosTelephone;

    private Boolean onlineStoresEmail;
    private Boolean onlineStoresTelephone;
}
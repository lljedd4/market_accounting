package com.kata.service;

import com.kata.model.Department;
import com.kata.repository.DepartmentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class DepartmentServiceImpl implements DepartmentService{
    private final DepartmentRepository repository;

    @Override
    public List<Department> findAllDepartments() {
        return repository.findAll();
    }
}

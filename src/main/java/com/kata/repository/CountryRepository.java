package com.kata.repository;

import com.kata.model.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @ In the name of Allah, most gracious and most merciful! 16.11.2022
 */
@Repository
public interface CountryRepository extends JpaRepository<Country, Long> {
    @Query("SELECT e FROM Country e WHERE e.isRemoved = false")
    List<Country> findAllNotRemoved();

    @Query("SELECT e FROM Country e WHERE e.id = :id AND e.isRemoved = false")
    Country findCountryByIdIfNotRemoved(Long id);

    @Modifying
    @Query("UPDATE Country SET isRemoved = true WHERE id = :id")
    void markCountryAsRemoved(Long id);
}

package com.kata.service;

import com.kata.model.Role;

import java.util.List;

public interface RoleService {

    List<Role> findAllRoles();
}

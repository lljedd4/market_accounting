package com.kata.service;

import com.kata.dto.UnitRequestDto;
import com.kata.dto.UnitResponseDto;

import java.util.List;

public interface UnitService {

    List<UnitResponseDto> getAllUnits();

    void create(UnitRequestDto unit);

    void deleteById(Long id);

    void edit(Long id, UnitRequestDto unit);

    UnitResponseDto findById(Long id);
}

package com.kata.dto;

import com.kata.model.TypeOfSalesChannel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SalesChannelDTO {
    private Long id;
    private String name;
    private TypeOfSalesChannel type;
    private String description;
}

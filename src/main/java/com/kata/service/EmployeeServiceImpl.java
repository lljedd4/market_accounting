package com.kata.service;

import com.kata.dto.EmployeeDTO;
import com.kata.mapper.EmployeeMapper;
import com.kata.model.Employee;
import com.kata.repository.EmployeeRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class EmployeeServiceImpl implements EmployeeService{
    private final EmployeeRepository repository;

    @Override
    public List<EmployeeDTO> findAll() {
        log.info("All Employees was found");
        return EmployeeMapper.INSTANCE.toDtoList(repository.findAllNotRemoved());
    }

    @Override
    public EmployeeDTO findById(Long id) {
        log.info("Employee with id = {} was found", id);
        return EmployeeMapper.INSTANCE.toDto(repository.findEmployeeByIdIfNotRemoved(id));
    }

    @Override
    @Transactional
    public void save(EmployeeDTO employeeDTO) {
        Employee employee = EmployeeMapper.INSTANCE.toEntity(employeeDTO);

        employee.setIsRemoved(false);
        repository.save(employee);
        log.info("Employee was saved: {}", employee);
    }

    @Override
    @Transactional
    public void update(Long id, EmployeeDTO employeeDTO) {
        Employee fromDto = EmployeeMapper.INSTANCE.toEntity(employeeDTO);
        Employee newEmployee = repository.findEmployeeByIdIfNotRemoved(id);

        newEmployee.setFirstName(fromDto.getFirstName());
        newEmployee.setMiddleName(fromDto.getMiddleName());
        newEmployee.setLastName(fromDto.getLastName());
        newEmployee.setTelephone(fromDto.getTelephone());
        newEmployee.setPost(fromDto.getPost());
        newEmployee.setIndividualTaxNumber(fromDto.getIndividualTaxNumber());
        newEmployee.setDescription(fromDto.getDescription());
        newEmployee.setLogin(fromDto.getLogin());
        newEmployee.setEmail(fromDto.getEmail());
        newEmployee.setDepartment(fromDto.getDepartment());
        newEmployee.setRole(fromDto.getRole());
        newEmployee.setAccessFromAddresses(fromDto.getAccessFromAddresses());
        newEmployee.setAccessFromNetwork(fromDto.getAccessFromNetwork());
        newEmployee.setNotifications(fromDto.getNotifications());
        newEmployee.setIsRemoved(false);
        repository.save(newEmployee);
        log.info("Employee was updated: {}", newEmployee);
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        repository.markEmployeeAsRemoved(id);
        log.info("Employee with id = {} was marked as removed", id);
    }
}

package com.kata.service;

import com.kata.dto.GroupOfProductsDTO;
import com.kata.mapper.GroupOfProductsMapper;
import com.kata.model.GroupOfProducts;
import com.kata.repository.GroupOfProductsRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class GroupOfProductsServiceImpl implements GroupOfProductsService {

    private final GroupOfProductsRepository groupOfProductsRepository;

    @Override
    public List<GroupOfProductsDTO> findAll() {
        log.info("Все группы товаров найдены");
        return GroupOfProductsMapper.INSTANCE.toDtoList(groupOfProductsRepository.findAllNotDeleted());
    }

    @Override
    public GroupOfProductsDTO findById(Long idGroup) {
        log.info("Group rate with id = {} was found", idGroup);
        return GroupOfProductsMapper.INSTANCE.toDto(groupOfProductsRepository.findGroupByIdIfNotDeleted(idGroup));
    }

    @Override
    @Transactional
    public void save(GroupOfProductsDTO groupOfProductsDTO) {
        GroupOfProducts groupOfProducts = GroupOfProductsMapper.INSTANCE.toEntity(groupOfProductsDTO);
        groupOfProducts.setRemoved(false);
        groupOfProductsRepository.save(groupOfProducts);
        log.info("Group rate was saved: {}", groupOfProducts);
    }

    @Override
    @Transactional
    public void update(Long idGroup, GroupOfProductsDTO groupOfProductsDTO) {
        GroupOfProducts fromDto = GroupOfProductsMapper.INSTANCE.toEntity(groupOfProductsDTO);
        GroupOfProducts newGroupOfProducts = groupOfProductsRepository.findGroupByIdIfNotDeleted(idGroup);
        newGroupOfProducts.setRemoved(false);
        newGroupOfProducts.setNameGroup(fromDto.getNameGroup());
        newGroupOfProducts.setDescriptionGroup(fromDto.getDescriptionGroup());
        newGroupOfProducts.setCodeGroup(fromDto.getCodeGroup());
        newGroupOfProducts.setExternalCodeGroup(fromDto.getExternalCodeGroup());
        newGroupOfProducts.setVat(fromDto.getVat());
        newGroupOfProducts.setTaxationSystem(fromDto.getTaxationSystem());
        newGroupOfProducts.setLastEditedBy(fromDto.getLastEditedBy());
        newGroupOfProducts.setLastEdited(fromDto.getLastEdited());
        groupOfProductsRepository.save(newGroupOfProducts);
        log.info("Group rate was updated: {}", newGroupOfProducts);
    }

    @Override
    @Transactional
    public void deleteById(Long idGroup) {
        groupOfProductsRepository.markGroupAsDeleted(idGroup);
        log.info("Group rate with id = {} was marked as deleted", idGroup);
    }
}
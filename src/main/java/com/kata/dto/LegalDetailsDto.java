package com.kata.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LegalDetailsDto {

    private Long id;

    private String typeOfCounterparty;

    private Long inn;

    private String fullName;

    private String legalAddress;

    private String commentToTheAddress;

    private Long kpp;

    private Long ogrn;

    private Long okpo;

}

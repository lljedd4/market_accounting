package com.kata.mapper;

import com.kata.dto.SalesChannelDTO;
import com.kata.model.SalesChannel;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface SalesChannelMapper {
    SalesChannelMapper INSTANCE = Mappers.getMapper(SalesChannelMapper.class);

    List<SalesChannelDTO> toDtoList(List<SalesChannel> vats);

    SalesChannelDTO toDto(SalesChannel vat);

    SalesChannel toEntity(SalesChannelDTO vatDto);
}

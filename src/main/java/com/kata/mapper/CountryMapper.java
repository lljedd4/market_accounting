package com.kata.mapper;

import com.kata.dto.CountryDTO;
import com.kata.model.Country;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @ In the name of Allah, most gracious and most merciful! 17.11.2022
 */
@Mapper(componentModel = "spring")
public interface CountryMapper {

    CountryDTO modelToDto(Country country);

    List<CountryDTO> modelsToDTOs(List<Country> countries);

    Country dtoToModel(CountryDTO countryDTO);
}

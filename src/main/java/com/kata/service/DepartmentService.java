package com.kata.service;

import com.kata.model.Department;

import java.util.List;

public interface DepartmentService {

    List<Department> findAllDepartments();
}
